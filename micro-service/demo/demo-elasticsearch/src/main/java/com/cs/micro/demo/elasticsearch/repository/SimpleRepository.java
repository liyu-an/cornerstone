/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: SimpleRepository
 * Author:   liyuan
 * Date:     2019-03-19 13:51
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.micro.demo.elasticsearch.repository;

import com.cs.micro.demo.elasticsearch.entity.Simple;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-03-19
 * @since 1.0.0
 */
@Component
public interface SimpleRepository extends ElasticsearchRepository<Simple, Integer> {

}