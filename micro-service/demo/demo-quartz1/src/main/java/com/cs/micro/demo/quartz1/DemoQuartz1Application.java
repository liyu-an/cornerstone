package com.cs.micro.demo.quartz1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoQuartz1Application
 * @since 2019-03-17 16:20
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
public class DemoQuartz1Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoQuartz1Application.class, args);
    }

}
