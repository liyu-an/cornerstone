package com.cs.micro.demo.quartz1.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoTask
 * @since 2019-03-17 16:36
 */
@Slf4j
@Component
public class DemoTask {

    @Scheduled(cron = "* * * * * *")
    public void test1() {
      log.debug("task1...");
    }

}
